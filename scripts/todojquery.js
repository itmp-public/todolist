function createTask(taskContent){
    $('ul').append('<li></li>');
    $('ul li').last()
    .append('<span class="fa fa-square-o fa-2x"></span>')
    .append('<span>' + taskContent + '</span>')
    .append('<span class="fa fa-times"></span>')
    .on('click', handleDone);
}

function handleDone(event){
    console.log($(event.currentTarget).children());
    $(event.currentTarget).addClass('done');
    $($(event.currentTarget).children()[0]).addClass('fa-check-square-o').removeClass('fa-square-o');
}

$('ul li').remove()