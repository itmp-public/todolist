/* function Task(options) {
    if(!options.content){
        throw new Error('Adjál meg egy leírást');
    }
    this.content = options.content;
    this.done = options.done || false;
    this.created = options.created || Date.now();
    this.id = options.id || Math.round(this.created * Math.random());
}

Task.prototype.markDone = function(){
    this.done = true;
}
*/
class Task {
    constructor(options){
        if(!options.content){
            throw new Error('Adjál meg egy leírást');
        }
        this.content = options.content;
        this.done = options.done || false;
        this.created = options.created || Date.now();
        this.id = options.id || Math.round(this.created * Math.random());
    }

    markDone() {
        this.done = true;
    }
}

let taskList = (function (array = []) {
    let taskArray = array;

    function findTaskByContent(content) {
        return taskArray.find(item => item.content === content);
    }

    function findTaskIndexByContent(content) {
        return taskArray.findIndex(item => item.content === content);
    }

    function findTaskById(id) {
        return taskArray.find(item => item.id === id);
    }

    function findTaskIndexById(id) {
        return taskArray.findIndex(item => item.id === id);
    }


    return {
        addTask(task) {
            // return taskArray.push(Object.assign({}, task));
            if(typeof task === 'string'){
                task
                .match(/[^;]*/g)
                .filter(item => item !== '')
                .forEach(item => {
                    taskArray.push(new Task({content: item.trim()}));
                });
                return taskArray.length;
            }
            return taskArray.push(new Task(task));
        },
        getTasks() {
            return taskArray.map(item => {
                return new Task(item);
            });
        },
        getTask(id) {
            const task = findTaskById(id);
            if (task) {
                return new Task(task);
            }
            return null;
        },
        modifyTask(task) {
            const idx = findTaskIndexById(task.id);
            if (idx > -1) {
                taskArray[idx] = new Task(task);
            }
        },
        removeTask(task) {
            const idx = findTaskIndexByContent(task.id);
            if (idx > -1) {
                taskArray.splice(idx, 1);
            }
        }
    }
})([new Task({content:'Hello', done: true})]);

taskList.addTask('Teendő1; Teendő2; Teendő3');
//console.log(taskList.getTasks());

console.log(JSON.parse(`[
    {
    "content": "Hello", 
    "done": true
    },
    {
        "content": "Teendő 1", 
        "done": false
    }
]`))

console.log(JSON.stringify(new Task({ content: 'Teendő JSON'})));
console.log(JSON.stringify(taskList.getTasks()));
/*
taskList.addTask(new Task({content: 'Ez egy teendő'}));
console.log(taskList.getTasks());
taskList.getTasks()[0].done = true;
let task = taskList.getTasks()[1];
console.log(taskList.getTasks());
console.log('==================');
console.log(taskList.getTask(task.id))
task = taskList.getTask(task.id);
task.markDone();
taskList.modifyTask(task);
console.log('==================');
console.log(console.log(taskList.getTasks()));
taskList.removeTask(task);
console.log('==================');
console.log(taskList.getTasks());

console.log('==================');
let task2 = taskList.getTask(taskList.getTasks()[0].id);

console.log(task.markDone === task2.markDone)
*/