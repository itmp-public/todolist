/**
 * <li class="done">
            <span class="fa fa-check-square-o fa-2x"></span>
            <span>This is an awesome ToDo template</span>
            <span class="fa fa-times"></span>
    </li>
 */

function createTask(taskContent) {
    let ul = document.querySelector('section > ul');
    let li = document.createElement("li");
    let checkIcon = document.createElement("span");
    let content = document.createElement("span");
    let deleteIcon = document.createElement("span");
    //let contentTextNode = document.createTextNode('rwere')

    checkIcon.classList.add('fa', 'fa-square-o', 'fa-2x');
    content.textContent = taskContent;
    deleteIcon.classList.add('fa', 'fa-times');
    // li.appendChild(checkIcon);
    li.appendChild(content);
    li.appendChild(deleteIcon);
    li.insertBefore(checkIcon, content);

    console.log(li);
    ul.appendChild(li);
    addEventListenerToCreatedListItem(li);
}

function createTaskWithLiteral(taskContent) {
    let htmlText = `
    <li>
        <span class="fa fa-square-o fa-2x"></span>
        <span>${taskContent}</span>
        <span class="fa fa-times"></span>
    </li>`;
    let ul = document.querySelector('section > ul');
    ul.insertAdjacentHTML('beforeend', htmlText);
    let li = ul.children[ul.children.length -1];
    addEventListenerToCreatedListItem(li);
}

function addEventListenerToCreatedListItem(listItemElement) {
    listItemElement.addEventListener('click', handleDone);
    listItemElement.lastElementChild.addEventListener('click', handleDelete);


}

function handleDone(event){
    markDone(event.currentTarget);
}

function handleAddButtonClick(event) {
    let inputElement = event.currentTarget.previousElementSibling;
    console.log(inputElement.value);
    if(inputElement.value){
        createTaskWithLiteral(inputElement.value);
    }
}

function handleFormSubmit(event){
    event.preventDefault();
    console.log('Form elküldése');
    let inputElement = document.querySelector('#todo');
    if(inputElement.value){
        createTaskWithLiteral(inputElement.value);
    }
}

function markDone(listItemElement) {
    listItemElement.classList.add("done");
    listItemElement.firstElementChild.classList.remove("fa-square-o");
    listItemElement.firstElementChild.classList.add("fa-check-square-o");
}

function removeTask(listItemElement) {
    listItemElement.remove();
}

function handleDelete(event) {
    removeTask(event.currentTarget.parentElement);
    event.stopPropagation();
}

// document.querySelectorAll('li').forEach(element => element.addEventListener('click', handleDone));

// document.querySelectorAll('li span:last-child').forEach(element => element.addEventListener('click', handleDelete));


document.querySelector('form button')
    .addEventListener('click', handleAddButtonClick);

document.querySelector('form')
    .addEventListener('submit', handleFormSubmit);

document.querySelectorAll('ul li').forEach(li => li.remove());